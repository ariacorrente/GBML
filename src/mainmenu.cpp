/*
=========================================================================
---[GBML - Main Menu functionalities]---
This file is part of Gaming Backup Multitool for Linux (or GBML for short).
Gaming Backup Multitool for Linux is available under the GNU GPL v3.0 license.
See the accompanying COPYING file for more details.
=========================================================================
*/

//-----------------------------------------------------------------------------------------------------------
// INCLUDED LIBRARIES
//-----------------------------------------------------------------------------------------------------------

// Standard C++/Qt libs
#include <QDir>
#include <QDirIterator>
#include <QFileDialog>
#include <QLabel>
#include <QListWidget>
#include <QMessageBox>
#include <QStorageInfo>
#include <QtDebug>
#include <QtGlobal>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

// Program-specific/external libs
#include "mainmenu.h"
#include "progressmenu.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "db.h"

//-----------------------------------------------------------------------------------------------------------
// VARIABLES
//-----------------------------------------------------------------------------------------------------------

// Chars for process mode (B = backup | R = restore) and option (S = saves | C = configs | G = games)
char MainMenu::ProcessMode = 0, MainMenu::ProcessOp = 0;

// Bool for knowing if the Mark/Unmark All buttons were used
bool MainMenu::MultiCheck;

// Strings for folder paths
QString MainMenu::CurrentSave, MainMenu::CurrentConfig, CurrentManifest; std::string MainMenu::BackupFolder = "";

qint64 RawSize; double TotalSize; // Numbers for raw (in bytes) and total size of operation

//-----------------------------------------------------------------------------------------------------------
// FUNCTIONS
//-----------------------------------------------------------------------------------------------------------

/* Enables/Disables buttons on the screen, as in:
   1) Start Button - User has set both a VALID Steam folder, a backup folder and at least ONE item is checked
   2) Mark/Unmark All - Game list has at least ONE valid item
   3) Backup Scan Buttons - User has set a VALID Steam library folder (the program checks if the path leads to the common folder)
   4) Restore Scan Buttons - User has set a backup folder AND, for each scanning button, its respective subfolder exists */
void MainMenu::LockUnlock(){
    // Block for 1)
    if (QDir(QString::fromStdString(DB::SteamPath)).dirName() != "common" || MainMenu::BackupFolder == "" || !MainMenu::OneItemChecked()){
        Window->ui->StartBtn->setDisabled(true);
    } else { Window->ui->StartBtn->setDisabled(false); }

    // Block for 2)
    if (Window->ui->GameList->count() == 0 || Window->ui->GameList->item(0)->text() == "No content found."){
        Window->ui->MarkAllBtn->setDisabled(true); Window->ui->UnmarkAllBtn->setDisabled(true);
    } else {
        Window->ui->MarkAllBtn->setDisabled(false); Window->ui->UnmarkAllBtn->setDisabled(false);
    }

    // Block for 3)
    if (QDir(Window->ui->SteamEntry->text()).dirName() != "common"){
        Window->ui->BackupScanSaveBtn->setDisabled(true);
        Window->ui->BackupScanConfigBtn->setDisabled(true);
        Window->ui->BackupScanGameBtn->setDisabled(true);
    } else {
        Window->ui->BackupScanSaveBtn->setDisabled(false);
        Window->ui->BackupScanConfigBtn->setDisabled(false);
        Window->ui->BackupScanGameBtn->setDisabled(false);
    }

    // Block for 4)
    if (Window->ui->BackupEntry->text() == ""){
        Window->ui->RestoreScanSaveBtn->setDisabled(true);
        Window->ui->RestoreScanConfigBtn->setDisabled(true);
        Window->ui->RestoreScanGameBtn->setDisabled(true);
    } else {
        if (QDir(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamSaves")).exists()){    // Scan for Saves
            Window->ui->RestoreScanSaveBtn->setDisabled(false);
        } else {
            Window->ui->RestoreScanSaveBtn->setDisabled(true);
        }

        if (QDir(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamConfigs")).exists()){  // Scan for Configs
            Window->ui->RestoreScanConfigBtn->setDisabled(false);
        } else {
            Window->ui->RestoreScanConfigBtn->setDisabled(true);
        }

        if (QDir(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamGames")).exists() &&   // Scan for Games
        QDir(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamManifests")).exists()){
            Window->ui->RestoreScanGameBtn->setDisabled(false);
        } else {
            Window->ui->RestoreScanGameBtn->setDisabled(true);
        }
    }
}

// Switches current process mode and option
void MainMenu::SwitchMode(char mode, char op){
    // Set text on screen accordingly
    if (mode == 'B'){
        Window->ui->CurrentMode->setText("BACKUP");
        switch (op){
            case 'S': Window->ui->CurrentOp->setText("SAVES"); break;
            case 'C': Window->ui->CurrentOp->setText("CONFIGS"); break;
            case 'G': Window->ui->CurrentOp->setText("GAMES"); break;
        }
    } else if (mode == 'R'){
        Window->ui->CurrentMode->setText("RESTORE");
        switch (op){
            case 'S': Window->ui->CurrentOp->setText("SAVES"); break;
            case 'C': Window->ui->CurrentOp->setText("CONFIGS"); break;
            case 'G': Window->ui->CurrentOp->setText("GAMES"); break;
        }
    }
    MainMenu::ProcessMode = mode; MainMenu::ProcessOp = op; // Set new process mode and option
}

// Read the last backup date of chosen option
void MainMenu::ReadBackupDate(char op){
    // Setting file name according to option
    QString filename;
    switch (op){
        case 'S': filename = Window->ui->BackupEntry->text() + "/LastSaveBackup.txt"; break;
        case 'C': filename = Window->ui->BackupEntry->text() + "/LastConfigBackup.txt"; break;
        case 'G': filename = Window->ui->BackupEntry->text() + "/LastGameBackup.txt"; break;
    }

    // Opening the file and reading content
    QFile file(filename); QString date, time;
    file.open(QIODevice::ReadOnly);
    QTextStream stream(&file);
    date = stream.readLine(); time = stream.readLine();
    file.close();

    Window->ui->LastBackupLabel->setText("Backup date:\n" + date + "\n" + time);    // Display the content in the UI
}

// Saves chosen directories
void MainMenu::SavePaths(){
    // If config folder doesn't exist, create it - alternatively, if a config file already exists, delete it
    if (!DB::GBMLConfigPath.exists()){ DB::GBMLConfigPath.mkdir(DB::GBMLConfigPath.absolutePath()); }
    else if (DB::GBMLConfigPath.exists("SavedPaths.txt")){ DB::GBMLConfigPath.remove("SavedPaths.txt"); }

    // Open a file and write in it both the current Steam folder and backup folder
    QFile file(DB::GBMLConfigPath.absolutePath() + "/SavedPaths.txt");
    file.open(QIODevice::ReadWrite);
    QTextStream stream(&file); stream << QString::fromStdString(DB::SteamPath) << endl << QString::fromStdString(MainMenu::BackupFolder) << endl;
    file.close();
}

// Loads chosen directories
void MainMenu::LoadPaths(){
    // If either the config folder or the config file is missing, end the function
    if (!DB::GBMLConfigPath.exists() || !QFile(DB::GBMLConfigPath.absolutePath() + "/SavedPaths.txt").exists()){ return; }

    // Open the config file and fetch both the current Steam folder and backup folder
    QFile file(DB::GBMLConfigPath.absolutePath() + "/SavedPaths.txt");
    file.open(QIODevice::ReadOnly);
    QTextStream stream(&file); DB::SteamPath = stream.readLine().toStdString(); MainMenu::BackupFolder = stream.readLine().toStdString();
    MainWindow::UpdatePaths();
    file.close();
}

// Update the GUI for scanning
void MainMenu::PrepareScan(){
    Window->ui->GameList->clear();                  // Clearing the list beforehand
    Window->ui->ScanningLabel->setVisible(true);    // Showing the scan message
}

// Functions that scan folders for content
// For Backup Mode - scans the system and/or the chosen Steam folder
void MainMenu::BackupScan(){
    QStringList NameList = DB::FetchColumn("SteamName");    // Fill a list with all games registered in the database
    bool GameAdded;                                         // Bool for knowing if a game has been added to the list

    // Start scan process based on option:
    // Saves
    if (MainMenu::ProcessOp == 'S'){
        for (int ct = 1; ct <= 3; ct++){                    // Iterate through all three possible save paths...
            foreach (QString GameName, NameList){           // ...for each game:
                GameAdded = false;                          // Set bool to false

                // Search for the respective save path, fetching the game folder as well, and convert the path to it's full extension
                MainMenu::CurrentSave = QString::fromStdString(DB::FetchGameInfo("SELECT SavePath" + std::to_string(ct) + " FROM RegisteredGames WHERE SteamName = \"" + GameName.toStdString() + "\""));

                // Only proceed if path is not any of the special labels
                if (MainMenu::CurrentSave != "[N/A]" && MainMenu::CurrentSave != "[UNKNOWN]" && MainMenu::CurrentSave != "[CLOUD-ONLY]"){
                    // Fetch respective game folder and replace labels
                    ProgressMenu::CurrentFolder = DB::FetchGameInfo("SELECT GameFolder FROM RegisteredGames WHERE SteamName = \"" + GameName.toStdString() + "\"");
                    DB::ReplaceLabels(MainMenu::CurrentSave);

                    // If path happens to end in a file or has a wildcard, convert path to its parent directory
                    if (QFileInfo(MainMenu::CurrentSave).isFile() || MainMenu::CurrentSave.contains('*')){ MainMenu::CurrentSave = QFileInfo(MainMenu::CurrentSave).dir().path(); }

                    // If full save path exists:
                    if (QDir(MainMenu::CurrentSave).exists()){
                        // Check if said game has been added to the list previously
                        for (int ct2 = 0; ct2 < Window->ui->GameList->count(); ct2++){
                            if (Window->ui->GameList->item(ct2)->text() == GameName){ GameAdded = true; }
                        }
                        if (GameAdded == false){ Window->ui->GameList->addItem(GameName); } // If it hasn't, add it to the list
                    }
                }
            }
        }

    // Configs
    } else if (MainMenu::ProcessOp == 'C'){
        for (int ct = 1; ct <= 3; ct++){                    // Iterate through all three possible config paths...
            foreach (QString GameName, NameList){           // ...for each game:
                GameAdded = false;                          // Set bool to false

                // Search for the respective config path, fetching the game folder as well, and convert the path to it's full extension
                MainMenu::CurrentConfig = QString::fromStdString(DB::FetchGameInfo("SELECT ConfigPath" + std::to_string(ct) + " FROM RegisteredGames WHERE SteamName = \"" + GameName.toStdString() + "\""));

                // Only proceed if path is not any of the special labels
                if (MainMenu::CurrentConfig != "[N/A]" && MainMenu::CurrentConfig != "[UNKNOWN]" && MainMenu::CurrentConfig != "[CLOUD-ONLY]"){
                    // Fetch respective game folder and replace labels
                    ProgressMenu::CurrentFolder = DB::FetchGameInfo("SELECT GameFolder FROM RegisteredGames WHERE SteamName = \"" + GameName.toStdString() + "\"");
                    DB::ReplaceLabels(MainMenu::CurrentConfig);

                    // If path happens to end in a file or has a wildcard, convert path to its parent directory
                    if (QFileInfo(MainMenu::CurrentConfig).isFile() || MainMenu::CurrentConfig.contains('*')){ MainMenu::CurrentConfig = QFileInfo(MainMenu::CurrentConfig).dir().path(); }

                    // If full config path exists:
                    if (QDir(MainMenu::CurrentConfig).exists()){

                        // Check if said game has been added to the list previously
                        for (int ct2 = 0; ct2 < Window->ui->GameList->count(); ct2++){
                            if (Window->ui->GameList->item(ct2)->text() == GameName){ GameAdded = true; }
                        }
                        if (GameAdded == false){ Window->ui->GameList->addItem(GameName); } // If it hasn't, add it to the list
                    }
                }
            }
        }

    // Games
    } else if (MainMenu::ProcessOp == 'G'){
        // Setting game iterator and manifest path
        QDirIterator GameIterator(QString::fromStdString(DB::SteamPath), QDir::Dirs | QDir::NoDotAndDotDot);
        QString ManifestPath = QString::fromStdString(DB::SteamPath);
        ManifestPath.remove("/common"); // Removing "/common"

        // Iterating through each game folder in steamapps:
        while (GameIterator.hasNext()){
            GameIterator.next();    // Go to next one

            // Fetch its respective AppID
            CurrentManifest = QString::fromStdString("appmanifest_" + DB::FetchGameInfo("SELECT AppID FROM RegisteredGames WHERE GameFolder = \"" + GameIterator.fileName().toStdString() + "\"") + ".acf");

            // If manifest file exists:
            if (QFileInfo(ManifestPath + "/" + CurrentManifest).exists()){
                // Fetch Steam name and add game to the list
                ProgressMenu::CurrentFolder = DB::FetchGameInfo("SELECT SteamName FROM RegisteredGames WHERE GameFolder = \"" + GameIterator.fileName().toStdString() + "\"");
                Window->ui->GameList->addItem(QString::fromStdString(ProgressMenu::CurrentFolder));
            }
        }
    }

    ProgressMenu::CurrentFolder = "";               // Cleaning folder variable
    Window->ui->LastBackupLabel->setText("");       // Finally, erase content from last backup label...
    Window->ui->ScanningLabel->setVisible(false);   // ...hide the scan message...
    Window->ui->TotalSizeLabel->setText("");        // ...and the total size message...
    Window->ui->TotalEntryLabel->setText("Items found:\n" + QString::number(Window->ui->GameList->count()));    // ...and show the total number of scanned items
}

// For Restore Mode - scans the chosen backup folder
void MainMenu::RestoreScan(){
    bool done = false;                              // Setting boolean for scanning

    // Starting scan process based on option:
    // Saves
    if (MainMenu::ProcessOp == 'S' && QDir(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamSaves")).exists()){
        // Setting save iterator
        QDirIterator SaveIterator(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamSaves"), QDir::Dirs | QDir::Hidden | QDir::NoDotAndDotDot);

        // Fetching content
        while (!done){
            if (SaveIterator.hasNext()){
                SaveIterator.next();    // Iterate to the next save folder

                // If folder is not empty and game exists in the database, there are saves to be copied - thus, add the game to the list
                if (QDir(SaveIterator.path()).count() > 0 && DB::FetchGameInfo("SELECT SteamName FROM RegisteredGames WHERE SteamName = \"" + SaveIterator.fileName().toStdString() + "\"") != "NONE"){
                    Window->ui->GameList->addItem(SaveIterator.fileName());
                }
            } else { done = true; }     // Exit loop if there are no more folders left
        }

    // Configs
    } else if (MainMenu::ProcessOp == 'C' && QDir(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamConfigs")).exists()){
        // Setting config iterator
        QDirIterator ConfigIterator(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamConfigs"), QDir::Dirs | QDir::Hidden | QDir::NoDotAndDotDot);

        // Fetching content
        while (!done){
            if (ConfigIterator.hasNext()){
                ConfigIterator.next();  // Iterate to the next config folder

                // If folder is not empty and game exists in the database, there are configs to be copied - thus, add the game to the list
                if (QDir(ConfigIterator.path()).count() > 0 && DB::FetchGameInfo("SELECT SteamName FROM RegisteredGames WHERE SteamName = \"" + ConfigIterator.fileName().toStdString() + "\"") != "NONE"){
                    Window->ui->GameList->addItem(ConfigIterator.fileName());
                }
            } else { done = true; }     // Exit loop if there are no more folders left
        }

    // Games
    } else if (ProcessOp == 'G' && QDir(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamGames")).exists() &&
                                   QDir(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamManifests")).exists()){
        // Setting game iterator
        QDirIterator GameIterator(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamGames"), QDir::Dirs | QDir::NoDotAndDotDot);

        // Fetching content
        while (!done){
            if (GameIterator.hasNext()){
                GameIterator.next();        // Iterate to the next game folder
                bool foundmanifest = false; // Setting flag for when the correct manifest is found

                // Search for respective AppID
                CurrentManifest = QString::fromStdString("appmanifest_" + DB::FetchGameInfo("SELECT AppID FROM RegisteredGames WHERE GameFolder = '" + GameIterator.fileName().toStdString() + "'") + ".acf");

                // (Re)setting manifest iterator every loop
                QDirIterator ManifestIterator(Window->ui->BackupEntry->text() + QString::fromStdString("/SteamManifests"), QDir::Files | QDir::NoDotAndDotDot);

                // Fetching respective manifest
                while (ManifestIterator.hasNext() && !foundmanifest){ 
                    ManifestIterator.next();    // Iterate to the next manifest file

                    // If the respective manifest file matches the game's manifest in the database, fetch Steam name, add game to list and exit inner loop
                    if (ManifestIterator.fileName() == CurrentManifest){
                        ProgressMenu::CurrentFolder = DB::FetchGameInfo("SELECT SteamName FROM RegisteredGames WHERE GameFolder = \"" + GameIterator.fileName().toStdString() + "\"");
                        Window->ui->GameList->addItem(QString::fromStdString(ProgressMenu::CurrentFolder));
                        foundmanifest = true;
                    }

                }
            } else { done = true; } // Exit outer loop if there are no more folders left
        }
    }
    MainMenu::ReadBackupDate(MainMenu::ProcessOp);  // Finally, scan for the backup date and time...
    Window->ui->ScanningLabel->setVisible(false);   // ...hide the scan message...
    Window->ui->TotalSizeLabel->setText("");        // ...and the total size message...
    Window->ui->TotalEntryLabel->setText("Items found:\n" + QString::number(Window->ui->GameList->count()));    // ...and show the total number of scanned items
}

// Function that organizes game list
void MainMenu::OrganizeGameList(){
    // If there's no items in the list, display a message
    if (Window->ui->GameList->count() == 0){ Window->ui->GameList->addItem("No content found."); }
    else {
        // Order the list by A-Z (alphabetical) and, for each game in the list, spawn a checkbox and set it as unchecked
        Window->ui->GameList->sortItems(Qt::AscendingOrder);
        for (int ct = 0; ct < Window->ui->GameList->count(); ct++){
            Window->ui->GameList->item(ct)->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
            Window->ui->GameList->item(ct)->setCheckState(Qt::Unchecked);
        }
    }
}

// Checks/Unchecks all games in the list
void MainMenu::CheckUncheck(bool check){
    // Iterate through each item in the list...
    for (int ct = 0; ct < Window->ui->GameList->count(); ct++){
        if (check == true){ Window->ui->GameList->item(ct)->setCheckState(Qt::Checked); }   // ...if unchecked, check...
        else { Window->ui->GameList->item(ct)->setCheckState(Qt::Unchecked); }              // ...if checked, uncheck
    }
}

// Checks if at least one item in the list was checked before starting backup/restore
bool MainMenu::OneItemChecked(){
    bool CheckConfirmed = false;    // Setting flag for when one checked item is found

    // Iterate through each item in the list - if one item is checked...
    for (int ct = 0; ct < Window->ui->GameList->count(); ct++)
        if (Window->ui->GameList->item(ct)->checkState() == Qt::Checked){ CheckConfirmed = true; } // ...change flag to "true"

    return CheckConfirmed;          // Returning the flag
}

// Removes items that are not checked from the list (for backup/restore process)
void MainMenu::RemoveUnchecked(){
    for (int ct = 0; ct < Window->ui->GameList->count(); ct++){ // Iterate through each item in the list and if it is unchecked...
        if (Window->ui->GameList->item(ct)->checkState() == Qt::Unchecked){ Window->ui->GameList->takeItem(ct); ct--; } // ...remove it from list and update counter
    }
}

// Calculates total size of checked items
void MainMenu::CheckTotalSize(){
    RawSize = 0; TotalSize = 0; QDir CurrentDir; QString CurrentPath, CurrentPathFolder;    // Reset total size value and create temps for path handling
    for (int ct = 0; ct < Window->ui->GameList->count(); ct++){                             // For each game in the list:
        if (Window->ui->GameList->item(ct)->checkState() == Qt::Checked){                   // If item is checked:
            // Filter by mode and option
            // For Backup Mode
            if (ProcessMode == 'B'){
                switch (ProcessOp){
                case 'S': case 'C':   // For Saves and Configs, operation is (almost) the same
                    for (int ct2 = 1; ct2 <= 3; ct2++){
                        // Find the respective path according to process option, fetch game folder, replace labels and reset filters
                        if (ProcessOp == 'S'){
                            CurrentPath = QString::fromStdString(DB::FetchGameInfo("SELECT SavePath" + std::to_string(ct2) + " FROM RegisteredGames"
                                          " WHERE SteamName = \"" + Window->ui->GameList->item(ct)->text().toStdString() + "\""));
                        } else if (ProcessOp == 'C'){
                            CurrentPath = QString::fromStdString(DB::FetchGameInfo("SELECT ConfigPath" + std::to_string(ct2) + " FROM RegisteredGames"
                                          " WHERE SteamName = \"" + Window->ui->GameList->item(ct)->text().toStdString() + "\""));
                        }
                        ProgressMenu::CurrentFolder = DB::FetchGameInfo("SELECT GameFolder FROM RegisteredGames WHERE SteamName = \"" + Window->ui->GameList->item(ct)->text().toStdString() + "\"");
                        DB::ReplaceLabels(CurrentPath); CurrentDir.setFilter(QDir::AllEntries); CurrentDir.setNameFilters(QStringList() << "*");

                        // Only proceed if path exists in the database
                        if (CurrentPath != "SKIP"){
                            CurrentDir.setPath(CurrentPath);                                                            // Set the temp dir
                            if (CurrentDir.absolutePath().contains('*')){                                               // If path has a wildcard:
                                CurrentDir.setNameFilters(QStringList() << CurrentDir.dirName()); CurrentDir.cdUp();    // Filter entries by name accordingly, and move up to parent directory

                                // Only proceed if there is at least one entry
                                if (!CurrentDir.entryInfoList().isEmpty()){
                                    // Depending on whether entries are files or folders, set filter accordingly
                                    if (CurrentDir.entryInfoList().at(0).isFile()){ CurrentDir.setFilter(QDir::Files | QDir::NoDotAndDotDot); }
                                    else { CurrentDir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot); }

                                    // If dir exists:
                                    if (CurrentDir.exists()){
                                        QDirIterator PathIterator(CurrentDir, QDirIterator::Subdirectories);                // Set an iterator to go to the next entry
                                        while (PathIterator.hasNext()){
                                            PathIterator.next();
                                            if (QFileInfo(PathIterator.filePath()).isFile()){                               // If entry is a file, proceed normally...
                                                RawSize += QFileInfo(PathIterator.filePath()).size();                       // ...adding each file's size to the total
                                            } else {                                                                        // If entry is a folder, set another iterator for it
                                                QDirIterator SubfolderIterator(PathIterator.filePath(), QDir::Files, QDirIterator::Subdirectories);
                                                while (SubfolderIterator.hasNext()){
                                                    SubfolderIterator.next(); RawSize += QFileInfo(SubfolderIterator.filePath()).size();
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {                                                                                // If there's no wildcard:
                                if (QFileInfo(CurrentDir.absolutePath()).isFile()){                                 // Check if path ends in a single file
                                    CurrentDir.setNameFilters(QStringList() << CurrentDir.dirName()); CurrentDir.cdUp(); // If yes, filter by name to avoid extra copying and move up to parent directory
                                }
                                CurrentDir.setFilter(QDir::Files | QDir::NoDotAndDotDot);                           // Set filter to files only
                                if (CurrentDir.exists()){                                                           // If dir exists:
                                    QDirIterator PathIterator(CurrentDir, QDirIterator::Subdirectories);            // Set an iterator...
                                    while (PathIterator.hasNext()){
                                        PathIterator.next(); RawSize += QFileInfo(PathIterator.filePath()).size();  // ...and add each file's size to the total
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 'G':   // For Games, it's way simpler: we only get the GameFolder and build the current path with Steam folder instead
                    ProgressMenu::CurrentFolder = DB::FetchGameInfo("SELECT GameFolder FROM RegisteredGames WHERE SteamName = \"" + Window->ui->GameList->item(ct)->text().toStdString() + "\"");
                    CurrentPath = QString::fromStdString(DB::SteamPath + "/" + ProgressMenu::CurrentFolder);
                    CurrentDir.setPath(CurrentPath); CurrentDir.setFilter(QDir::Files | QDir::NoDotAndDotDot);
                    if (CurrentDir.exists()){
                        QDirIterator PathIterator(CurrentDir, QDirIterator::Subdirectories);
                        while (PathIterator.hasNext()){
                            PathIterator.next(); RawSize += QFileInfo(PathIterator.filePath()).size();
                        }
                    }
                    break;
                }
            // For Restore Mode
            } else if (ProcessMode == 'R'){
                switch (ProcessOp){
                case 'S': case 'C':   // For Saves and Configs: build the path with backup folder and subfolder + SteamName + save/config path's last folder
                    for (int ct2 = 1; ct2 <= 3; ct2++){
                        if (ProcessOp == 'S'){
                            CurrentPath = Window->ui->BackupEntry->text() + QString::fromStdString("/SteamSaves/") + Window->ui->GameList->item(ct)->text() + "/";
                            CurrentPathFolder = QString::fromStdString(DB::FetchGameInfo("SELECT SavePath" + std::to_string(ct2) + " FROM RegisteredGames"
                                                " WHERE SteamName = \"" + Window->ui->GameList->item(ct)->text().toStdString() + "\""));
                        } else if (ProcessOp == 'C'){
                            CurrentPath = Window->ui->BackupEntry->text() + QString::fromStdString("/SteamConfigs/") + Window->ui->GameList->item(ct)->text() + "/";
                            CurrentPathFolder = QString::fromStdString(DB::FetchGameInfo("SELECT ConfigPath" + std::to_string(ct2) + " FROM RegisteredGames"
                                                " WHERE SteamName = \"" + Window->ui->GameList->item(ct)->text().toStdString() + "\""));
                        }

                        // Fetch the respective game folder and replace labels to get absolute save/config path
                        ProgressMenu::CurrentFolder = DB::FetchGameInfo("SELECT GameFolder FROM RegisteredGames WHERE SteamName = \"" + Window->ui->GameList->item(ct)->text().toStdString() + "\"");
                        DB::ReplaceLabels(CurrentPathFolder);
                        if (QDir(CurrentPathFolder).dirName().contains("*") || QFileInfo(CurrentPathFolder).isFile()){  // If path ends in a wildcard or a file:
                            CurrentPathFolder.remove("/" + QDir(CurrentPathFolder).dirName());                          // Remove the entry with the wildcard so only the parent dir remains
                            CurrentPath += QDir(CurrentPathFolder).dirName();                                           // Add the last dir (which should be the correct Save/ConfigFolder) to full path
                        } else {                                                                                        // If not, then fetch Save/ConfigFolder directly from the database
                            if (ProcessOp == 'S'){
                                CurrentPathFolder = QString::fromStdString(DB::FetchGameInfo("SELECT SaveFolder" + std::to_string(ct2) + " FROM RegisteredGames"
                                                    " WHERE SteamName = \"" + Window->ui->GameList->item(ct)->text().toStdString() + "\""));
                            } else if (ProcessOp == 'C'){
                                CurrentPathFolder = QString::fromStdString(DB::FetchGameInfo("SELECT ConfigFolder" + std::to_string(ct2) + " FROM RegisteredGames"
                                                    " WHERE SteamName = \"" + Window->ui->GameList->item(ct)->text().toStdString() + "\""));
                            }
                            CurrentPath += CurrentPathFolder;                                                           // Add the folder normally to the path
                        }

                        // Only proceed if no special labels are found
                        if (!CurrentPath.contains("[N/A]") && !CurrentPath.contains("[UNKNOWN]") && !CurrentPath.contains("[CLOUD-ONLY]")){
                            CurrentDir.setPath(CurrentPath); CurrentDir.setFilter(QDir::Files | QDir::NoDotAndDotDot);  // Set the temp dir and filters
                            if (CurrentDir.exists()){                                                                   // If dir exists:
                                QDirIterator PathIterator(CurrentDir, QDirIterator::Subdirectories);                    // Iterate through it...
                                while (PathIterator.hasNext()){
                                    PathIterator.next(); RawSize += QFileInfo(PathIterator.filePath()).size();          // ...and add each file's size to the total
                                }
                            }
                        }
                    }
                    break;
                case 'G':   // For Games: almost the same thing as Backup Games, with backup folder instead of Steam folder
                    CurrentPath = Window->ui->BackupEntry->text() + QString::fromStdString("/SteamGames/") +
                                  QString::fromStdString(DB::FetchGameInfo("SELECT GameFolder FROM RegisteredGames WHERE SteamName = \"" +
                                  Window->ui->GameList->item(ct)->text().toStdString() + "\""));
                    CurrentDir.setPath(CurrentPath); CurrentDir.setFilter(QDir::Files | QDir::NoDotAndDotDot);
                    if (CurrentDir.exists()){
                        QDirIterator PathIterator(CurrentDir, QDirIterator::Subdirectories);
                        while (PathIterator.hasNext()){
                            PathIterator.next(); RawSize += QFileInfo(PathIterator.filePath()).size();
                        }
                    }
                    break;
                }
            }
        }
    }

    // Display total value as bytes by default
    TotalSize = (double)RawSize;
    Window->ui->TotalSizeLabel->setText("Approx. size:\n" + QString::number(TotalSize) + " bytes");

    // If it exceeds 1024 bytes, convert to KB
    if (TotalSize > 1024){
        TotalSize = TotalSize / 1024;
        Window->ui->TotalSizeLabel->setText("Approx. size:\n" + QString::number(TotalSize, 'f', 2) + " KB");
    }
    // If it exceeds 1024KB, convert to MB
    if (TotalSize > 1024){
        TotalSize = TotalSize / 1024;
        Window->ui->TotalSizeLabel->setText("Approx. size:\n" + QString::number(TotalSize, 'f', 2) + " MB");
    }
    // If it exceeds 1024MB, convert to GB (stops here)
    if (TotalSize > 1024){
        TotalSize = TotalSize / 1024;
        Window->ui->TotalSizeLabel->setText("Approx. size:\n" + QString::number(TotalSize, 'f', 2) + " GB");
    }
}

// Checks if there's enough space on target path before starting the process
bool MainMenu::CheckDiskSpace(){
    double TotalBytes = 0.00; bool HasSpace = false;

    // Converting total size to bytes for comparison
    if (Window->ui->TotalSizeLabel->text().contains("GB")){ TotalBytes = TotalSize * (1024 * 1024 * 1024); }
    else if (Window->ui->TotalSizeLabel->text().contains("MB")){ TotalBytes = TotalSize * (1024 * 1024); }
    else if (Window->ui->TotalSizeLabel->text().contains("KB")){ TotalBytes = TotalSize * 1024; }

    // Checking if free space in target path is bigger than the total of bytes needed for process
    if (ProcessMode == 'B'){
        if (QStorageInfo(Window->ui->BackupEntry->text()).bytesFree() > TotalBytes){ HasSpace = true; } else { HasSpace = false; }
    } else if (ProcessMode == 'R'){
        if (QStorageInfo::root().bytesFree() > TotalBytes){ HasSpace = true; } else { HasSpace = false; }
    }
    return HasSpace;
}
